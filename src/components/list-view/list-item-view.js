import React from 'react'

const ListItemView = ({ item }) =>
  <li>{item}</li>
ListItemView.propTypes = {
  item: React.PropTypes.number.isRequired
}

export default ListItemView
