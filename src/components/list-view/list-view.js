import React from 'react'
import {ListItemView} from './list-item-view.js'

const ListView = ({ items }) => items.map((item) => { return <ListItemView item={item}/> })

ListView.propTypes = {
  items: React.PropTypes.array.isRequired
}

export default ListView
